import { createRouter, createWebHashHistory } from 'vue-router'

import EmployeeList from '../modules/employee/pages/EmployeeList'

const routes = [
    { 
        path: '/',
        component: EmployeeList,
        name: "Home",
    },
    { 
        path: '/create',
        component: () => import(/* webpackChunkName: "create" */ '@/modules/employee/pages/EmployeeCreate')
    },
    { 
        path: '/edit/:id',
        component: () => import(/* webpackChunkName: "edit" */ '@/modules/employee/pages/EmployeeEdit')
    },
    { 
        path: '/:pathMatch(.*)*',
        component: EmployeeList
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes, // short for `routes: routes`
})

export default router