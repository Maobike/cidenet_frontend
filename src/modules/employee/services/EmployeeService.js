import { axiosInstance } from "@/api/AxiosConfig";

class EmployeeService {

    /**
     * @returns un empleado
     * @author Mauricio Moreno @maomaoq@hotmail.com
     */

    getEmployee = (id) => {
        return axiosInstance.get(`/api/employees/${id}`)
    }

    /**
     * @returns empleados
     * @author Mauricio Moreno @maomaoq@hotmail.com
     */

    getEmployees = () => {
        return axiosInstance.get('/api/employees')
    }

    /**
     * @returns empleados
     * @author Mauricio Moreno @maomaoq@hotmail.com
     */

    postEmployees = (data) => {
        return axiosInstance.post('/api/employees', data)
    }

    /**
     * @returns empleados
     * @author Mauricio Moreno @maomaoq@hotmail.com
     */

    putEmployees = (id, data) => {
        return axiosInstance.put(`/api/employees/${id}`, data)
    }

}

export default new EmployeeService();